import { 
    ON_SELECT_CAR_BRAND ,
    ON_CLICK_2018_FILTER_YEAR,
    ON_CLICK_2019_FILTER_YEAR,
    ON_CLICK_2020_FILTER_YEAR

} from "../constant/carFilter.constant"

export const onFilterCarBrand = (value) => {
    return{
        type: ON_SELECT_CAR_BRAND,
        payload: value
    }
} 
export const onFilterYear2018 = () => {
    return{
        type: ON_CLICK_2018_FILTER_YEAR
    }
}

export const onFilterYear2019 = () => {
    return{
        type: ON_CLICK_2019_FILTER_YEAR
    }
}

export const onFilterYear2020 = () => {
    return{
        type: ON_CLICK_2020_FILTER_YEAR
    }
}