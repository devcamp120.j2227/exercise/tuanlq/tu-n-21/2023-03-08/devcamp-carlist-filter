import { Typography , Grid, MenuItem, Select, Button, Card, CardMedia, CardContent} from "@mui/material";
import { Container } from "@mui/system";
import { useSelector, useDispatch } from "react-redux";
import { onFilterCarBrand, onFilterYear2018, onFilterYear2019, onFilterYear2020 } from "../actions/carFilter.actions";
export function CarList () {
    const {brand, carList} = useSelector(value => value.carListReducer);
    const dispatch = useDispatch();
    const handlerFilterYear2018 = () => {{
        dispatch(onFilterYear2018())
    }}
    const handlerFilterYear2019 = () => {{
        dispatch(onFilterYear2019())
    }}
    const handlerFilterYear2020 = () => {{
        dispatch(onFilterYear2020())
    }}
    const handlerFilterBrand = (event) =>{
        dispatch(onFilterCarBrand(event.target.value));
    }
    return(
        <Container>
            <Grid container>
                <Grid item md={12} xs={12} sm={12} lg={12} sx={{display:"flex", justifyContent:"center"}}> 
                    <Typography variant="h2" >Car List Filter 🚗🚙🚓</Typography>
                </Grid>
                <Grid item md={12} xs={12} sm={12} lg={12} mt={5} sx={{display:"flex", justifyContent:"center"}}> 
                    <Typography sx={{fontSize:"20px"}}>Filter by Brand 🔎: </Typography>
                    <Select
                        value={brand}
                        size="small"
                        sx={{
                            marginLeft:"20px",
                            width:"200px",
                        }}
                        onChange={handlerFilterBrand}
                    >   
                        <MenuItem value={"none"}>All</MenuItem>
                        <MenuItem value={"BMW"}>BMW</MenuItem>
                        <MenuItem value={"Audi"}>Audi</MenuItem>
                        <MenuItem value={"VW"}>VW</MenuItem>
                    </Select>
                </Grid>
                <Grid item md={12} xs={12} sm={12} lg={12} mt={5} sx={{display:"flex", justifyContent:"center"}}> 
                    <Typography sx={{fontSize:"20px"}}>Filter by Year 🔎: </Typography>
                </Grid>
                <Grid item md={12} xs={12} sm={12} lg={12} mt={5} sx={{display:"flex", justifyContent:"center", gap:"20px"}}> 
                    <Button variant="contained"  sx={{backgroundColor: "purple"}}  onClick={handlerFilterYear2018}>2018</Button>
                    <Button variant="contained" sx={{ backgroundColor: "orange"}}  onClick={handlerFilterYear2019}>2019</Button>
                    <Button variant="contained" sx={{ backgroundColor: "yellow", color:"black"}}  onClick={handlerFilterYear2020}>2020</Button>
                </Grid>
                <Grid item md={12} xs={12} sm={12} lg={12} mt={5} sx={{display:"flex", justifyContent:"center", gap:"20px", flexWrap:"wrap"}}> 
                    {carList.map ((element, index) => {
                       return <Card sx={{ width: "300px"}} key={index}>
                                    <CardMedia 
                                        sx={{height: "200px"}}
                                        image={element.url}
                                    />
                                    <CardContent>
                                        <Typography sx={{fontWeight:"600"}} mb={1}>Name: <span  style={{fontWeight:"300 "}}>{element.name}</span></Typography>
                                        <Typography  sx={{fontWeight:"600"}}>Year: <span  style={{fontWeight:"300"}}>{element.release_year}</span></Typography>
                                    </CardContent>
                                </Card>
                    }
                    )}
                </Grid>
            </Grid>
        </Container>
    )
}