import { combineReducers } from "redux";
import carListReducer from "./carFilter.reducer";

const rootReducer = combineReducers({
    carListReducer
});

export default rootReducer;

