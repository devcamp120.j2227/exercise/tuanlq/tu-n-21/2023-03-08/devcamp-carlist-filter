import { 
    ON_SELECT_CAR_BRAND,
    ON_CLICK_2018_FILTER_YEAR,
    ON_CLICK_2019_FILTER_YEAR,
    ON_CLICK_2020_FILTER_YEAR
} from "../constant/carFilter.constant";
const initialState = {
    carList: [
        {
            name: "BMW M6",
            url:
              "https://mediapool.bmwgroup.com/cache/P9/201411/P90169551/P90169551-the-new-bmw-m6-coup-exterior-12-2014-600px.jpg",
            release_year: 2020
          },
          {
            name: "VW Polo",
            url:
              "https://cdn.euroncap.com/media/30740/volkswagen-polo-359-235.jpg?mode=crop&width=359&height=235",
            release_year: 2018
          },
          {
            name: "Audi S6",
            url:
              "https://www.motortrend.com/uploads/sites/5/2020/03/6-2020-audi-s6.jpg?fit=around%7C875:492.1875",
            release_year: 2020
          },
          {
            name: "BMW M2",
            url:
              "https://imgd.aeplcdn.com/0x0/cw/ec/37092/BMW-M2-Exterior-141054.jpg?wm=0",
            release_year: 2019
          },
          {
            name: "Audi A3",
            url: "https://cdn.motor1.com/images/mgl/BEooZ/s3/2021-audi-s3.jpg",
            release_year: 2019
          }
    ],
    carListClone:[
        {
            name: "BMW M6",
            url:
              "https://mediapool.bmwgroup.com/cache/P9/201411/P90169551/P90169551-the-new-bmw-m6-coup-exterior-12-2014-600px.jpg",
            release_year: 2020
          },
          {
            name: "VW Polo",
            url:
              "https://cdn.euroncap.com/media/30740/volkswagen-polo-359-235.jpg?mode=crop&width=359&height=235",
            release_year: 2018
          },
          {
            name: "Audi S6",
            url:
              "https://www.motortrend.com/uploads/sites/5/2020/03/6-2020-audi-s6.jpg?fit=around%7C875:492.1875",
            release_year: 2020
          },
          {
            name: "BMW M2",
            url:
              "https://imgd.aeplcdn.com/0x0/cw/ec/37092/BMW-M2-Exterior-141054.jpg?wm=0",
            release_year: 2019
          },
          {
            name: "Audi A3",
            url: "https://cdn.motor1.com/images/mgl/BEooZ/s3/2021-audi-s3.jpg",
            release_year: 2019
          }
    ], 
    brand: "none",
    year: "none"
};

const carListReducer = ( state = initialState, action) =>{
    switch (action.type){
        case ON_SELECT_CAR_BRAND:
            state.brand = action.payload;
            state.carList = [];
            for(let i = 0 ; i < state.carListClone.length; i ++){
                if( state.carListClone[i].name.split(" ")[0] === state.brand){
                    state.carList.push(state.carListClone[i])
                }
            }
            if(state.brand === "none"){
                state.carList = state.carListClone
            }
            break;
        case ON_CLICK_2018_FILTER_YEAR:
            state.carList = [];
            for(let i = 0 ; i < state.carListClone.length; i ++){
                if( state.carListClone[i].release_year === 2018){
                    state.carList.push(state.carListClone[i])
                }
            }
            break;
        case ON_CLICK_2019_FILTER_YEAR:
                state.carList = [];
                for(let i = 0 ; i < state.carListClone.length; i ++){
                    if( state.carListClone[i].release_year === 2019){
                        state.carList.push(state.carListClone[i])
                    }
                }
                break;
        case ON_CLICK_2020_FILTER_YEAR:
            state.carList = [];
            for(let i = 0 ; i < state.carListClone.length; i ++){
                if( state.carListClone[i].release_year === 2020){
                    state.carList.push(state.carListClone[i])
                }
            }
            break;
        default:
            break;
    }
    return{...state};
}

export default carListReducer;